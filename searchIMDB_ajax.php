<?php

require_once 'config.php';

use Symfony\Component\DomCrawler\Crawler;

if (!empty($_REQUEST)) {
    $html = file_get_contents('https://www.imdb.com/find?q=' . urlencode($_REQUEST['keyword']) . '&ref_=nv_sr_sm');
    $explode = explode('<div id="main">', $html);
    if (!empty($explode) && isset($explode[1])) {
        $newHtml = $explode[1];
        if (!empty($newHtml)) {
            $crawler = new Crawler($newHtml);
            $t = $crawler->filter('div.findSection')->eq(0)->filter('table.findList > tr');
            $resultSearch = $t->each(
                function (Crawler $node, $i) {
                    try {
                        $img = $node->filter('td.primary_photo > a > img')->attr('src');
                    } catch (Exception $e) {
                        $img = '';
                    }
                    try {
                        $title = $node->filter('td.result_text > a')->html();
                    } catch (Exception $e) {
                        $title = '';
                    }
                    try {
                        $href = $node->filter('td.result_text > a')->attr('href');
                    } catch (Exception $e) {
                        $href = '';
                    }
                    return
                        [
                            "title" => $title,
                            "href" => $href,
                            "img" => $img
                        ];
                }
            );
            echo json_encode($resultSearch);
            die();
        }
    }
}