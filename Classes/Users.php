<?php

namespace Classes;

class Users
{
    public static function getUserInfo($userId)
    {
        $sql = "SELECT
                         `user_name`,
                         `user_email`,
                         `user_watching_time`
                FROM
                    `users` 
                WHERE
                    `user_id` =:user_id";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId
            ]
        );
        return $stmt->fetchAll();
    }
    public static function createCodeRestore($email, $code): bool
    {
        $sql = "UPDATE `users` 
                SET `user_code_restore` =:code
                WHERE
                    `user_email` =:email";

        $stmt = Db::getInstance()->prepare($sql);
        return $stmt->execute(
            [
                "code" => $code,
                "email" => $email
            ]
        );
    }
    public static function getUserIdByEmail($email){
        $sql = "SELECT
                         `user_id`
                FROM
                    `users` 
                WHERE
                    `user_email` =:email";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "email" => $email
            ]
        );
        return $stmt->fetch();

    }
}