<?php

namespace Classes;

class Db
{
    private static $instance;

    private function __construct()
    {
        self::$instance = new \PDO(
            'mysql:host=localhost;port=3306;dbname=' . $_ENV['DB_NAME'] . ';charset=utf8',
            $_ENV['DB_USER'],
            $_ENV['DB_PASSWORD'],
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            ]
        );
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            new self();
        }
        return self::$instance;
    }
}