<?php

namespace Classes;

class Movies
{
    public static function checkMovie(int $userId): int
    {
        $sql = "SELECT
                    `movie_id` 
                FROM
                    movies 
                WHERE
                    `movie_user_id` =:user_id 
                    LIMIT 1";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId
            ]
        );
        return $stmt->fetchColumn();
    }

    public static function getMovieByUserId(int $userId, int $page, int $perPage = 4): array
    {
        $sql = "SELECT
                    `movie_id`,
                    `movie_title`,
                    `movie_href`,
                    `movie_img`,
                    `movie_category_id`
                FROM
                    `movies` 
                WHERE
                    `movie_user_id` =:user_id
        LIMIT " . $perPage . " OFFSET " . ($page - 1) * $perPage;
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId
            ]
        );
        return $stmt->fetchAll();
    }

    public static function getMovieByCategory(int $userId, int $categoryId, int $page, int $perPage = 4): array
    {
        $sql = "SELECT
                    `movie_id`,
                    `movie_title`,
                    `movie_href`,
                    `movie_img`
                FROM
                    `movies` 
                WHERE
                    `movie_user_id` =:user_id
                  AND `movie_category_id` =:category_id
        LIMIT " . $perPage . " OFFSET " . ($page - 1) * $perPage;
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId,
                "category_id" => $categoryId
            ]
        );
        return $stmt->fetchAll();
    }

    public static function getCountMovies(int $userId): int
    {
        $sql = "SELECT
                    COUNT( 1 ) 
                FROM
                    movies 
                WHERE
                    movie_user_id =:user_id";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId
            ]
        );
        return $stmt->fetchColumn();
    }

    public static function getCountMoviesByCategory(int $userId, int $categoryId): int
    {
        $sql = "SELECT
                    COUNT( 1 ) 
                FROM
                    movies 
                WHERE
                    movie_user_id =:user_id
                    AND `movie_category_id` =:category_id";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId,
                "category_id" => $categoryId
            ]
        );
        return $stmt->fetchColumn();
    }

    public static function addMovie(
        string $mTitle,
        string $mHref,
        string $mImg,
        int $mUserId,
        int $mCategoryId,
        string $mStoryline,
        float $mRating,
        string $mOriginTitle,
        string $mRuntime,
        string $mYear
    ): bool {
        $sql = "INSERT INTO `movies`(
                          `movie_title`,
                          `movie_href`,
                          `movie_img`,
                          `movie_user_id`,  
                          `movie_category_id`,
                          `movie_storyline`,
                          `movie_rating`,
                          `movie_origin_title`,
                          `movie_runtime`,
                          `movie_year`
    )
    VALUES(
           :mTitle,
           :mHref,
           :mImg,
           :mUserId,
           :mCategoryId,
           :mStoryline,
           :mRating,
           :mOriginTitle,
           :mRuntime,   
           :mYear
    )";
        $stmt = Db::getInstance()->prepare($sql);
        return $stmt->execute(
            [
                "mTitle" => $mTitle,
                "mHref" => $mHref,
                "mImg" => $mImg,
                "mUserId" => $mUserId,
                "mCategoryId" => $mCategoryId,
                "mStoryline" => $mStoryline,
                "mRating" => $mRating,
                "mOriginTitle" => $mOriginTitle,
                "mRuntime" => $mRuntime,
                "mYear" => $mYear
            ]
        );
    }

    public static function deleteMovie(int $movieId): bool
    {
        $sql = "DELETE FROM `movies` WHERE `movie_id`=:movieId;";
        $stmt = Db::getInstance()->prepare($sql);
        return $stmt->execute(
            [
                "movieId" => $movieId
            ]
        );
    }

    public static function checkMovieInDb(int $userId, string $link): bool
    {
        $sql = "SELECT
                    `movie_id` 
                FROM
                    movies 
                WHERE
                    `movie_href` =:link AND 
                    `movie_user_id` =:userId 
                    LIMIT 1";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "link" => $link,
                "userId" => $userId
            ]
        );
        return $stmt->fetchColumn();
    }

    public static function getSearchMovie(int $userId, string $title)
    {
        $sql = "SELECT
                    `movie_id`,
                    `movie_title`,
                    `movie_href`,
                    `movie_img`,
                    `movie_category_id`
                FROM
                    `movies` 
                WHERE
                    `movie_user_id` =:user_id AND 
                    (`movie_title` =:title OR 
                    `movie_origin_title` =:title)";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_id" => $userId,
                "title" => $title
            ]
        );
        return $stmt->fetch();
    }

    public static function getFullMovieInfo($userId, $link)
    {
        $sql = "SELECT
                  *
                FROM
                    movies 
                WHERE
                    `movie_href` =:link AND 
                    `movie_user_id` =:userId";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "link" => $link,
                "userId" => $userId
            ]
        );
        return $stmt->fetch();
    }

    public static function updateMovie(
        string $mTitle,
        string $mYear,
        int $mCategory,
        int $mUserId,
        string $mLink,
        string $mImg,
        string $mStoryline,
        float $mRating,
        string $mOriginTitle,
        string $mRuntime
    ): bool {
        $sql = "UPDATE `movies` 
                SET 
                    `movie_title` =:mTitle,
                    `movie_year` =:mYear,
                    `movie_category_id` =:mCategory,
                    `movie_img` =:mImg,
                    `movie_storyline` =:mStoryline,
                    `movie_rating` =:mRating,
                    `movie_origin_title` =:mOriginTitle,
                    `movie_runtime` =:mRuntime
                WHERE
                    `movie_user_id` =:movieUserId AND
                    `movie_href` =:movieLink";
        $stmt = Db::getInstance()->prepare($sql);
        return $stmt->execute(
            [
                "mTitle" => $mTitle,
                "mYear" => $mYear,
                "mCategory" => $mCategory,
                "mImg" => $mImg,
                "mStoryline" => $mStoryline,
                "mRating" => $mRating,
                "mOriginTitle" => $mOriginTitle,
                "mRuntime" => $mRuntime,
                "movieUserId" => $mUserId,
                "movieLink" => $mLink
            ]
        );
    }
    public static function getAllMovie(int $userId): array
    {
        $sql = "SELECT
                    `movie_title`,
                    `movie_origin_title` 
                FROM
                    movies 
                WHERE
                    `movie_user_id` =:user_id";
//        " AND (
//                    `movie_origin_title` LIKE CONCAT('%', :text, '%')
//                    OR `movie_title` LIKE CONCAT('%', :text, '%')
//                    OR `movie_storyline`   LIKE  CONCAT('%', :text, '%')) ";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
//                "text" => $text,
                "user_id" => $userId
            ]
        );
        $response =[];
        while ($row = $stmt->fetch()) {
            $response[] = $row['movie_title'];
            $response[] = $row['movie_origin_title'];
        }
        return $response;
        //return $stmt;//->fetch(\PDO::FETCH_ASSOC);
    }

}













