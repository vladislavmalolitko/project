<?php

namespace Classes;

use \Classes\Db;

class Verification
{
    private const MIN_LENGTH = 4;
    private const MAX_LENGTH = 255;
    private const SALT = 'cdrf58erfr43v5erve$';


    public static function checkName($name)
    {
        if (strlen($name) < self::MIN_LENGTH) {
            return "Name should be longer";
        }
        if (strlen($name) > self::MAX_LENGTH) {
            return "Name should be shorter";
        }
        if (!ctype_alpha($name)) {
            return "Name should be without numbers";
        }
        return false;
    }

    public static function checkingUsedEmail($email)
    {
        $sql = "SELECT
                `user_email`
            FROM
                `users`
            WHERE
               `user_email` = :email";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "email" => $email
            ]
        );
        return $stmt->fetchAll();
    }

    public static function checkEmail($email, $register)
    {
        if (strlen($email) > self::MAX_LENGTH) {
            return "Name should be shorter";
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return "Email is not valid";
        }
        if ($register) {
            if (self::checkingUsedEmail($email)) {
                return "Email is already used";
            }
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) < self::MIN_LENGTH) {
            return "Password should be longer";
        }
        if (strlen($password) > self::MAX_LENGTH) {
            return "Password should be shorter";
        }
        if (!ctype_alnum($password) || ctype_digit($password) || ctype_alpha($password)) {
            return "Password should be with numbers and letters";
        }
        return false;
    }

    private static function encryptPassword(string $password): string
    {
        return sha1($password . self::SALT);
    }

    public static function updatePassword(string $email,string $password): int
    {
        $sql = "UPDATE `users`
                SET `user_password` =:password
                WHERE `user_email` =:email";
        $stmt = Db::getInstance()->prepare($sql);
        return $stmt->execute(
            [
                "email" => $email,
                "password" => self::encryptPassword($password)
            ]
        );
    }

    public static function registerUser(string $name, string $email, string $password, float $watching_time = 0): int
    {
        $sql = "INSERT INTO `users`(
                                           `user_name`,
                                           `user_email`,
                                           `user_password`,
                                           `user_watching_time`
                    )
                    VALUES(
                           :user_name,
                           :user_email,
                           :user_password,
                           :user_watching_time
                    )";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "user_name" => $name,
                "user_email" => $email,
                "user_password" => self::encryptPassword($password),
                "user_watching_time" => $watching_time
            ]
        );
        return Db::getInstance()->lastInsertId();
    }

    public static function loginUser(string $email, string $password)
    {
        $sql = "SELECT * 
                FROM 
                `users`
                WHERE 
                      `user_email` = :email AND
                      `user_password` = :password";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "email" => $email,
                "password" => self::encryptPassword($password)
            ]
        );
        $user = $stmt->fetch();
        return (!empty($user) ? $user : []);
    }

    public static function checkCode($email, $code){
        $sql = "SELECT * 
                FROM 
                `users`
                WHERE 
                      `user_email` = :email AND
                      `user_code_restore` = :code";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "email" => $email,
                "code" => $code
            ]
        );
        return $stmt->fetch();
    }
}