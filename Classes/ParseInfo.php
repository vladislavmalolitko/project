<?php

namespace Classes;
use Symfony\Component\DomCrawler\Crawler;

class ParseInfo
{
    public static function getCrawler($link)
    {
        $movieLink = 'https://www.imdb.com/' . $link;
        $html = file_get_contents($movieLink);

        if ($html) {
            return new Crawler($html);
        }
        return false;
    }
    public static function getImg(Crawler $crawler): ?string
    {
        try {
            return $crawler->filter('div[data-testid="hero-media__poster"] > div')->eq(0)->filter('img')->attr('src');
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getTitle(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('h1[data-testid="hero-title-block__title"]')->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getOriginTitle(Crawler $crawler): string
    {
        try {
            $originalTitle = trim($crawler->filter('div[data-testid="hero-title-block__original-title"]')->html());
            $originalTitle = explode(":", $originalTitle);
            return trim($originalTitle[1]);
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getYear(Crawler $crawler): string
    {
        try {
            return ($crawler->filter('span[class="TitleBlockMetaData__ListItemText-sc-12ein40-2 jedhex"]')->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getRating(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('div[data-testid="hero-rating-bar__aggregate-rating__score"] > span')->eq(0)->html());
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getRuntime(Crawler $crawler): string
    {
        try {
            $runtime = trim($crawler->filter('div.TitleBlock__TitleMetaDataContainer-sc-1nlhx7j-2 > ul > li ')->last()->text());
            if ((!strstr($runtime, 'h')) && (!strstr($runtime, 'min'))) {
                return 'No info';
            }
            return $runtime;
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getStoryline(Crawler $crawler): string
    {
        try {
            return trim($crawler->filter('div[data-testid="storyline-plot-summary"] > div>div')->text());
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function getCategory(Crawler $crawler)
    {
        try {
            $category = trim($crawler->filter('ul[data-testid="hero-title-block__metadata"]>li')->eq(0)->text());
            if (strstr($category, 'TV Series')) {
                return 3;
            } else {
                $category = false;
            }
        } catch (\Exception $e) {
            $category = false;
        }
        if (!$category) {
            try {
                $category = trim($crawler->filter('li[data-testid="storyline-genres"]>div>ul>li')->eq(0)->text());
                if (strstr($category, 'Animation')) {
                    return 2;
                } else {
                    $category = false;
                }
            } catch (\Exception $e) {
                $category = false;
            }
            if (!$category) {
                return 1;
            }
        }
    }
    public static function getAll(Crawler $crawler): array
    {
        $img = ParseInfo::getImg($crawler);
        $title = ParseInfo::getTitle($crawler);
        $originalTitle = ParseInfo::getOriginTitle($crawler);
        $year = ParseInfo::getYear($crawler);
        $rating = ParseInfo::getRating($crawler);
        $runtime = ParseInfo::getRuntime($crawler);
        $storyline = ParseInfo::getStoryline($crawler);
        $category = ParseInfo::getCategory($crawler);

        return [
            'img' => $img,
            'title' => $title,
            'originTitle' => $originalTitle,
            'year' => $year,
            'rating' => $rating,
            'runtime' => $runtime,
            'storyline' => $storyline,
            'category' => $category
        ];
    }

}