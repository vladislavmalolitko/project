<?php

namespace Classes;

class FileCheck
{

    public static function checkType($mime)
    {
        $fileExtension = array('image/jpeg', 'image/png');
        if (in_array($mime, $fileExtension)) {
            return false;
        }
        return 'The uploaded file is not a picture';
    }

    public static function checkName($name): string
    {
        if (file_exists($name)) {
            $dir = pathinfo($name, PATHINFO_DIRNAME);
            $newName = pathinfo($name, PATHINFO_FILENAME);
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            $dirName = $dir . DIRECTORY_SEPARATOR . $newName . '1.' . $extension;
            return self::checkName($dirName);
        }
        return $name;
    }

    public static function checkError($errorCode)
    {
        if ($errorCode !== UPLOAD_ERR_OK) {
            $errorMessages = [
                UPLOAD_ERR_INI_SIZE => 'File size exceeded upload_max_filesize in PHP config.',
                UPLOAD_ERR_FORM_SIZE => 'Upload file size exceeded MAX_FILE_SIZE value in HTML form.',
                UPLOAD_ERR_PARTIAL => 'The download file was received only partially.',
                UPLOAD_ERR_NO_FILE => 'The file was not uploaded.',
                UPLOAD_ERR_NO_TMP_DIR => 'Temp folder is missing.',
                UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
                UPLOAD_ERR_EXTENSION => 'PHP extension stopped uploading file.'
            ];
            $unknownMessage = 'An unknown error occurred while uploading the file.';
            return $errorMessages[$errorCode] ?? $unknownMessage;
        } else {
            return false;
        }
    }

    public static function checkSize($size)
    {
        if ($size == 0) {
            return 'The file is too large.';
        } else {
            return false;
        }
    }
}