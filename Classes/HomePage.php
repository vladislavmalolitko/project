<?php

namespace Classes;

class HomePage
{

    public static function getCountPages(int $userId, $category): int
    {
        if ($category == 'all') {
            $countMovies = Movies::getCountMovies($userId);
        } else {
            $countMovies = Movies::getCountMoviesByCategory($userId, $category);
        }
        return ceil($countMovies / 4);
    }

    public static function getMovie(int $userId, $category, int $page): array
    {
        if ($category == 'all') {
            return Movies::getMovieByUserId($userId, $page);
        } else {
            return Movies::getMovieByCategory($userId, $category, $page);
        }
    }
    public static function autoload($query)
    {

        $data = array();
        $condition = preg_replace('/[^a-zA-Z0-9\- ]','' , $query);
        var_dump($condition);
        $sql = "
        SELECT `movie_title`
        FROM movies
        WHERE `movie_title`:=condition 
        ORDER BY `movie_id` DESC 
        LIMIT 10";
       $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "condition" => $condition
            ]
        );
        $replaceString = '<br>'.$condition.'</br>';

        foreach ($stmt as $row){
            $data[]=array(
                'movie_title'=>str_ireplace($condition, $replaceString, $row["movie_title"])
            );
         }

        return json_encode($data);
    }
}
