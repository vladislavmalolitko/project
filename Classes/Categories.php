<?php

namespace Classes;

class Categories
{
    public static function getCategory()
    {
        $sql = "SELECT
                    *
                FROM
                    `categories`";
        $stmt = Db::getInstance()->query($sql);
        return $stmt->fetchAll();
    }
    public static function getCategoryById(int $categoryId)
    {
        $sql = "SELECT
                    `category_name`
                FROM
                    `categories`
                WHERE `category_id` =:categoryId";
        $stmt = Db::getInstance()->prepare($sql);
        $stmt->execute(
            [
                "categoryId" => $categoryId
            ]
        );
        return $stmt->fetch();

    }

}