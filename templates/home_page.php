<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!-- HOME-->
                <li class="nav-item active">
                    <a class="navbar-brand" href="/index.php?link=all">LocalHomeMovieDB <span class="sr-only">(current)</span></a>
                </li>
                <!-- END HOME-->
                <li class="nav-item">
                    <a class="nav-link " href="/search_movie.php">Search movie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/user_info.php">My profile</a>
                </li>

                <!-- Single button -->
                <li class="nav-item">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle nav-link" type="button" id="dropdownMenu1"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="true">
                            Categories
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php
                            echo '<li><a class="nav-link" href="/index.php?link=all">All</a></li>';
                            if (!empty($categories)) {
                                foreach ($categories as $category) {
                                    echo '<li><a class="nav-link" href="/index.php?link=' . $category['category_id'] . '">' . $category['category_name'] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </li>

            </ul>
            <form class="form-inline my-2 my-lg-0" action="/index.php" method="post">

                <input class="form-control mr-sm-2" name="searchInDb" id="search_field" type="search"
                       placeholder="Search"
                       aria-label="Search" autocomplete="off">
                <span id="search_result"></span>
                <div class="btn-group mr-2" role="group">
                    <button class="btn btn-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
                </div>
            </form>

        </div>
    </nav>
</header>

<main role="main">
    <form action="/index.php" method="post">

        <div class="album py-5 bg-light">
            <div class="container">
                <div class="row">
                    <?php
                    if (!empty($movies)):
                    foreach ($movies as $movie): ?>
                        <div class="col-xl-3 col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="film-img"
                                     src="<?php
                                     echo $movie['movie_img']; ?>"
                                     alt="<?php
                                     echo $movie['movie_title']; ?>">
                                <div class="card-body">
                                    <p class="card-text"><?php
                                        echo $movie['movie_title']; ?></p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="col text-center">
                                            <div class="btn-group">
                                                <button type="submit" name="view" value="<?php
                                                echo $movie['movie_href']; ?>"
                                                        class="btn btn-sm btn-outline-success">
                                                    View
                                                </button>
                                                <button type="submit" name="delete" value="<?php
                                                echo $movie['movie_id']; ?>" class="btn btn-sm btn-outline-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach; ?>
                </div>
                <div class="row align-items-center">
                    <div class="col text-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <?php
                                if ($countPages > 1) : ?>
                                    <li class="page-item">
                                        <button type="submit" class="page-link" name="prev">Previous</button>
                                    </li>
                                    <?php
                                    if ($countPages < 5) :
                                        for ($i = 1; $i <= $countPages; $i++) :
                                            ?>
                                            <li class="page-item <?php
                                            if ($status == $i) {
                                                echo 'active';
                                            } ?>">
                                                <button type="submit" class="page-link" name="page" value="<?php
                                                echo $i; ?>"><?php
                                                    echo $i; ?></button>
                                            </li>
                                        <?php
                                        endfor;
                                    else: ?>
                                        <li class="page-item <?php if($status == 1){
                                            echo 'active';
                                        }?>">
                                            <button type="submit" class="page-link" name="page" value="1">1</button>
                                        </li>
                                        <?php
                                        if (($_SESSION['page'] > 2) && ($_SESSION['page'] < $countPages-1)) {
                                            $i = $_SESSION['page'];
                                        } else {
                                            if($_SESSION['page'] == 2){
                                                $i = $_SESSION['page'];
                                                $i++;
                                            }
                                            if($_SESSION['page'] == 1){
                                                $i = $_SESSION['page'];
                                                $i+=2;
                                            }
                                            if($_SESSION['page'] == $countPages){
                                                $i = $_SESSION['page'];
                                                $i-=2;
                                            }
                                            if($_SESSION['page'] == $countPages-1){
                                                $i = $_SESSION['page'];
                                                $i--;
                                            }
                                        }
                                        $i--;
                                        $count = 0;
                                        while ($count != 3):?>
                                            <li class="page-item <?php if($status == $i){
                                                echo 'active';
                                            }?>">
                                                <button type="submit" class="page-link" name="page" value="<?php
                                                echo $i; ?>"><?php
                                                    echo $i; ?></button>
                                            </li>
                                        <?php
                                        $i++;
                                        $count++;
                                        endwhile; ?>
                                        <li class="page-item <?php if($status == $i){
                                            echo 'active';
                                        }?>">
                                            <button type="submit" class="page-link" name="page" value="<?php
                                            echo $countPages ?>"><?php
                                                echo $countPages ?></button>
                                        </li>
                                    <?php
                                    endif; ?>

                                    <li class="page-item">
                                        <button type="submit" class="page-link" name="next">Next</button>
                                    </li>
                                <?php
                                endif;
                                ?>
                            </ul>
                        </nav>
                    </div>
                </div>

                <?php
                else:?>
                    <div class="container">
                        <div class="row justify-content-center">
                            <?php
                            echo '<h1>No movies added</h1>'; ?>
                        </div>
                    </div>
                <?php
                endif; ?>
            </div>
        </div>
    </form>


</main>
</body>

<?php
require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>
