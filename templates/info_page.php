<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!-- HOME-->
                <li class="nav-item">
                    <a class="navbar-brand" href="/index.php?link=all">LocalHomeMovieDB <span class="sr-only">(current)</span></a>
                </li>
                <!-- END HOME-->
                <li class="nav-item">
                    <a class="nav-link" href="/search_movie.php">Search movie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/user_info.php">My profile</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" action="/index.php" method="post">
                <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
            </form>
        </div>
    </nav>
</header>

<main role="main">
    <?php
    if (!empty($movieInfo)) { ?>

        <form action="/movie_info.php" id="formAction" method="post" enctype="multipart/form-data">
            <?php
            if (isset($_POST['saveMovie']) || isset($_POST['saveAddingMovie'])) {
                if (!empty($errors)) {
                    ?>
                    <div class="alert alert-danger text-center alert-dismissible fade show" role="alert">
                        <?php
                        foreach ($errors as $error): ?>

                            <h6><?php
                                echo $error; ?></h6>

                        <?php
                        endforeach; ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                } else { ?>
                    <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
                        <h6 class="alert-heading">Well done!</h6>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <?php
                }
            }
            if (isset($_POST['addMovie'])) { ?>
                <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
                    <h6 class="alert-heading">Successful added!</h6>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
            } ?>


            <div class="card bg-light align-items-start">
                <article class="card-body mx-auto" style="max-width: 1000px;">
                    <div class="row">
                        <?php
                        if (!isset($_POST['editMovie'])) { ?>

                            <div class="col-12 text-center">
                                <h1 class="display-4"><?php
                                    echo $movieInfo['title'] . ' (' . $movieInfo['year'] . ')'; ?></h1><br>
                            </div>
                            <?php
                        } ?>
                        <div class="col-5">
                            <div class="row ">
                                <div class="col text-center">
                                    <input type="hidden" class="imgName" name="img" value="<?php
                                    echo $movieInfo['img'];
                                    ?>">
                                    <img id="img" src="<?php
                                    echo $movieInfo['img'];
                                    ?>"></div>
                                <div class="w-100"></div>
                                <div class="col text-center">
                                    <?php
                                    if (isset($_POST['editMovie'])) {
                                        ?>
                                        <div class="col">
                                            <input type="file" id="file" name="file">
                                            <div id="drop-area">
                                                <h3 class="drop-text" id="drop-area-text">Drag and Drop Images Here</h3>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col">
                                            <button type="button" name="img" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                        <?php
                                    }
                                    ?></div>
                            </div>
                        </div>

                        <div class="col-7">
                            <div class="form-group" style="max-width: 500px;">
                                <?php
                                if (isset($_POST['editMovie'])) { ?>
                                    <h5>Title:</h5>
                                    <div class="row">
                                        <div class="col-5">
                                            <input name="title" type="text" value="<?php
                                            echo $movieInfo['title'] ?>"></div>
                                        <div class="col">
                                            <button type="button" name="title" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>
                                    <h5>Year:</h5>
                                    <div class="row">
                                        <div class="col-5">
                                            <input name="year" type="text" value="<?php
                                            echo $movieInfo['year'] ?>"></div>
                                        <div class="col">
                                            <button type="button" name="year" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>

                                    <h5>Origin title</h5>
                                    <div class="row">
                                        <div class="col-5">
                                            <input name="originTitle" type="text" value="<?php
                                            echo $movieInfo['originTitle'] ?>"></div>
                                        <div class="col-7">
                                            <button type="button" name="originTitle" value="<?php
                                            echo $_SESSION['link']; ?>" class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>
                                    <h5>Category:</h5>
                                    <div class="row">
                                        <div class="col-5">
                                            <select class="form-select form-select-sm"
                                                    aria-label=".form-select-sm example"
                                                    name="category">
                                                <?php
                                                if (!empty($categories)) {
                                                    foreach ($categories as $category) { ?>
                                                        <option <?php
                                                        if ($category['category_id'] == $movieInfo['category']) {
                                                            echo 'selected';
                                                        }
                                                        ?> value="<?php
                                                        echo $category['category_id']; ?>"><?php
                                                            echo $category['category_name']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </div>
                                        <div class="col-7">
                                            <button type="button" name="category" value="<?php
                                            echo $_SESSION['link']; ?>" class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>
                                    <h5>Rating: </h5>
                                    <div class="row">
                                        <div class="col-5">
                                            <input name="rating" type="text" value="<?php
                                            echo $movieInfo['rating'] ?>"></div>
                                        <div class="col-7">
                                            <button type="button" name="rating" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>
                                    <h5>Runtime: </h5>

                                    <div class="row">
                                        <div class="col-5">
                                            <input name="runtime" type="text" value="<?php
                                            echo $movieInfo['runtime'] ?>"></div>
                                        <div class="col-7">
                                            <button type="button" name="runtime" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>
                                    <h5>Storyline: </h5>
                                    <div class="row">
                                        <div class="col-11">
                                    <textarea name="storyline" rows="5" cols="60"><?php
                                        echo $movieInfo['storyline'] ?></textarea>
                                        </div>
                                        <div class="col-1 text-center">
                                            <button type="button" name="storyline" value="<?php
                                            echo $_SESSION['link']; ?>"
                                                    class="refresh btn btn-secondary btn-sm">
                                                Refresh
                                            </button>
                                        </div>
                                    </div>


                                    <?php
                                } else { ?>

                                    <h5>Origin title</h5>
                                    <p><?php
                                        echo $movieInfo['originTitle'] ?></p>
                                    <h5>Category:</h5>
                                    <p><?php
                                        foreach ($categories as $category) {
                                            if ($category['category_id'] == $movieInfo['category']) {
                                                echo $category['category_name'];
                                            }
                                        } ?></p>
                                    <h5>Rating: </h5>
                                    <p><?php
                                        echo $movieInfo['rating'] ?></p>

                                    <h5>Runtime: </h5>
                                    <p><?php
                                        echo $movieInfo['runtime'] ?></p>

                                    <h5>Storyline: </h5>
                                    <p><?php
                                        echo $movieInfo['storyline'] ?></p>
                                    <?php
                                } ?>
                            </div>
                        </div>


                    </div>
                </article>

                <div class="form-group container text-center">
                    <?php
                    if (!isset($_POST['editMovie'])) { ?>
                        <button type="submit" name="editMovie" class="btn btn-primary">Edit</button>
                        <?php
                    }
                    if ($availabilityMovie) {
                        if (isset($_POST['editMovie'])) {
                            ?>
                            <button type="button" name="refreshAll" value="<?php
                            echo $_SESSION['link']; ?>"
                                    class="refresh btn btn-info">
                                Refresh
                            </button>
                            <button type="submit" name="saveAddingMovie" id="saveAddingMovie" class="btn btn-success">
                                Save Adding
                            </button>
                            <?php
                        }
                    } else {
                        if (isset($_POST['editMovie'])) {
                            ?>
                            <button type="button" name="refreshAll" value="<?php
                            echo $_SESSION['link']; ?>"
                                    class="refresh btn btn-info">
                                Refresh
                            </button>
                            <button type="submit" name="saveMovie" id="saveMovie" class="btn btn-success">Save</button>
                            <?php
                        }
                        if (!isset($_POST['editMovie'])) {
                            if (!isset($_POST['addMovie'])) { ?>
                                <button type="submit" name="addMovie" class="btn btn-success">Add</button>
                                <?php
                            }
                        }
                    } ?>
                </div>
            </div>
        </form>

        <?php
    } else { ?>
        <div class="container">
            <div class="row justify-content-center">
                <h1>No info about movie</h1>
            </div>
        </div>
        <?php
    } ?>
</main>

<?php
require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>
