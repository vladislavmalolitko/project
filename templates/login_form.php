<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
    <main role="main">
        <div class="card bg-light">
            <article class="card-body mx-auto" style="max-width: 400px;">
                <h4 class="card-title mt-3 text-center">Log In</h4>

                <form action="/login.php" method="post">
                    <!-- EMAIL -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" class="form-control <?php
                        echo(!empty($error['email']) ? 'is-invalid' : ''); ?>"
                               placeholder="Email address" type="email" value="<?php
                        echo ($_POST['email']) ?? '' ?>">
                        <div class="invalid-feedback">
                            <?php
                            echo($error['email'] ?? ''); ?>
                        </div>
                    </div>

                    <!-- PASSWORD -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password" class="form-control <?php
                        echo(!empty($error['password']) ? 'is-invalid' : ''); ?>"
                               placeholder="Create password" type="password" value="<?php
                        echo ($_POST['password']) ?? '' ?>">
                        <div class="invalid-feedback">
                            <?php
                            echo($error['password'] ?? ''); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Sing In</button>
                    </div>
                    <p class="text-center">Create an account? <a href="/register.php">Register</a></p>
                    <p class="text-center">Forgot your password? <a href="/restore.php">Restore</a></p>
                </form>
            </article>
        </div>

    </main>

<?php
require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>