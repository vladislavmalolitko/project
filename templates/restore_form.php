<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
    <main role="main">
        <div class="card bg-light">
            <article class="card-body mx-auto" style="max-width: 400px;">


                <form action="/restore.php" method="post">
                    <h4 class="card-title mt-3 text-center">Restore password</h4>

                    <?php
                    if (isset($_POST['restore']) && (((($_POST['restore'] == 'restorePassword') && empty($error['email']))) || (($_POST['restore'] == 'checkCode') && !empty($error['code'])))) {
                        ?>

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                            </div>
                            <input name="code" class="form-control <?php
                            echo(!empty($error['code']) ? 'is-invalid' : ''); ?>"
                                   placeholder="Code restore" value="<?php
                            echo ($_POST['code']) ?? '' ?>">
                            <div class="invalid-feedback">
                                <?php
                                echo($error['code'] ?? ''); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" name="restore" value="checkCode"
                                    class="btn btn-primary btn-block"> Confirmation code
                            </button>
                        </div>
                        <?php
                    }
                    if (isset($_POST['restore']) && ((($_POST['restore'] == 'checkCode') && empty($error['code'])) || (($_POST['restore'] == 'changePassword') && (!empty($error['password']) || !empty($error['confirm_password'])) ))) {
//
                        ?>

                        <!-- PASSWORD -->
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <input name="password" class="form-control <?php
                            echo(!empty($error['password']) ? 'is-invalid' : ''); ?>"
                                   placeholder="Create password" type="password" value="<?php
                            echo ($_POST['password']) ?? '' ?>">
                            <div class="invalid-feedback">
                                <?php
                                echo($error['password'] ?? ''); ?>
                            </div>
                        </div>

                        <!-- RE-PASSWORD -->
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <input name="confirm_password" class="form-control <?php
                            echo(!empty($error['confirm_password']) ? 'is-invalid' : ''); ?>"
                                   placeholder="Repeat password" type="password" value="<?php
                            echo ($_POST['confirm_password']) ?? '' ?>">
                            <div class="invalid-feedback">
                                <?php
                                echo($error['confirm_password'] ?? ''); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="restore" value="changePassword"
                                    class="btn btn-primary btn-block"> Change password
                            </button>
                        </div>
                        <?php
//                        }
                    } //else {
                    if (!isset($_POST['restore']) || (isset($_POST['restore']) && ($_POST['restore'] == 'restorePassword')) && !empty($error['email'])) {
                        ?>

                        <!-- EMAIL -->

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                            </div>
                            <input name="email" class="form-control <?php
                            echo(!empty($error['email']) ? 'is-invalid' : ''); ?>"
                                   placeholder="Email address" type="email" value="<?php
                            echo ($_POST['email']) ?? '' ?>">
                            <div class="invalid-feedback">
                                <?php
                                echo($error['email'] ?? ''); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="restore" value="restorePassword"
                                    class="btn btn-primary btn-block"> Send confirmation code
                            </button>
                        </div>

                        <?php
                    } ?>


                    <p class="text-center">Create an account? <a href="/register.php">Register</a></p>
                </form>
            </article>
        </div>

    </main>

<?php
require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>