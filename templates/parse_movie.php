<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!-- HOME-->
                <li class="nav-item">
                    <a class="navbar-brand" href="/index.php?link=all">LocalHomeMovieDB <span
                                class="sr-only">(current)</span></a>
                </li>
                <!-- END HOME-->
                <li class="nav-item active">
                    <a class="nav-link active" href="#">Search movie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/user_info.php">My profile</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" action="/index.php" method="post">
                <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
            </form>
        </div>
    </nav>
</header>
<main role="main">
    <form action="/search_movie.php" method="post">
        <! NAME -->
        <h5 class="card-title mt-3 text-center">Enter the movie you want to watch</h5>
        <article class="card-body mx-auto" style="max-width: 400px;">
            <div class="typeaheadDiv form-group input-group justify-content-md-center mx-auto">
                <input type="text" class="typeahead form-control" id="searchInIMDB"
                       placeholder="Enter movie"
                       name="movieName" autocomplete="off">
            </div>
        </article>
    </form>

</main>
</body>
</html>
