<?php

require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-secondary">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- HOME-->
                    <li class="nav-item">
                        <a class="navbar-brand" href="/index.php?link=all">LocalHomeMovieDB<span class="sr-only">(current)</span></a>
                    </li>
                    <!-- END HOME-->
                    <li class="nav-item">
                        <a class="nav-link" href="/search_movie.php">Search movie</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">My profile</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="/index.php" method="post">
                    <button type="submit" name="logout" class="btn btn-danger btn-block"> Log out</button>
                </form>
            </div>
        </nav>
    </header>

    <main role="main">
        <form action="/search_movie.php" method="post">
            <div class="card bg-light">
                <article class="card-body mx-auto" style="max-width: 1000px;">
                    <div class="form-group container ">
                        <?php
                        if (!empty($userInfo)) {
                            foreach ($userInfo as $user) {
                                echo '<h1 class="display-4">Name: ' . $user['user_name'] . '</h1><br>';
                                echo '<h1 class="display-4">Email: ' . $user['user_email'] . '</h1><br>';
                                echo '<h1 class="display-4">Total movie watching time: ' . $user['user_watching_time'] . '</h1>';
                            }
                        } else {
                            echo '<h1 class="display-4">No info about user</h1>';
                        }
                        ?>
                    </div>
                </article>
            </div>
        </form>
    </main>

<?php
require_once TEMPLATES_PATH . "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>