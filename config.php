<?php

define("ROOT_PATH", dirname(__FILE__, 1));
const CLASSES_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "Classes" . DIRECTORY_SEPARATOR;
const FUNCTIONS_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "functions" . DIRECTORY_SEPARATOR;
const TEMPLATES_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR;

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);
