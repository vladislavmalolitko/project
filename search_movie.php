<?php

require_once "config.php";
if (empty($_SESSION)) {
    header("Location: /register.php");
    die();
}

require_once TEMPLATES_PATH . "parse_movie.php";
