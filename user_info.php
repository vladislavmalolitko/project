<?php

require_once "config.php";

use Classes\Users;

if (empty($_SESSION)) {
    header("Location: /register.php");
    die();
}
$userInfo = Users::getUserInfo($_SESSION['user_id']);

require_once TEMPLATES_PATH . "info_of_user.php";