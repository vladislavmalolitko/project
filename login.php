<?php

require_once "config.php";

use \Classes\Verification;

$error = [];
if (!empty($_POST)) {
    foreach ($_POST as $key => $value) {
        if (empty($value)) {
            $error[$key] = "Field should be filled";
            continue;
        }
        if ($key === "email") {
            if ($rezError = Verification::checkEmail($value, false)) {
                $error[$key] = $rezError;
            }
            continue;
        }
        if ($key === "password") {
            if ($rezError = Verification::checkPassword($value)) {
                $error[$key] = $rezError;
            }
        }
    }
    if (empty($error)) {
        $user = Verification::loginUser($_POST['email'], $_POST['password']);
        if (!empty($user)) {
            $_SESSION['user_id'] = $user['user_id'];
            header("Location: /index.php");
            die();
        } else {
            $error['email'] = "Wrong email or password";
        }
    }
}
require_once TEMPLATES_PATH . "login_form.php";
