<?php

require_once "config.php";

use \Classes\Verification;

$error = [];
if (!empty($_POST)) {
    foreach ($_POST as $key => $value) {
        if (empty($value)) {
            $error[$key] = "Field should be filled";
            continue;
        }
        if ($key === "name") {
            if ($rezError = Verification::checkName($value)) {
                $error[$key] = $rezError;
            }
            continue;
        }
        if ($key === "email") {
            if ($rezError = Verification::checkEmail($value, true)) {
                $error[$key] = $rezError;
            }
            continue;
        }
        if ($key === "password") {
            if ($rezError = Verification::checkPassword($value)) {
                $error[$key] = $rezError;
            }
            continue;
        }
        if ($_POST['password'] != $_POST['confirm_password']) {
            $error[$key] = "Password don't match";
        }
    }
    if (empty($error)) {
        $user = Verification::registerUser($_POST['name'], $_POST['email'], $_POST['password']);
        if (!empty($user)) {
            $_SESSION['user_id'] = $user;
            header("Location: /index.php");
            die();
        } else {
            echo "Empty user";
        }
    }
}
require_once TEMPLATES_PATH . "register_form.php";