<?php

require_once "config.php";
use Classes\ParseInfo;

if (!empty($_POST)) {
    $crawler = ParseInfo::getCrawler($_POST['link']);
    switch ($_POST['name']){
        case 'title':
            $title = ParseInfo::getTitle($crawler);
            echo json_encode($title);
            die();
        case 'year':
            $year = ParseInfo::getYear($crawler);
            echo json_encode($year);
            die();
        case 'category':
            $category = ParseInfo::getCategory($crawler);
            echo json_encode($category);
            die();
        case 'originTitle':
            $originTitle = ParseInfo::getOriginTitle($crawler);
            echo json_encode($originTitle);
            die();
        case 'rating':
            $rating = ParseInfo::getRating($crawler);
            echo json_encode($rating);
            die();
        case 'runtime':
            $runtime = ParseInfo::getRuntime($crawler);
            echo json_encode($runtime);
            die();
        case 'storyline':
            $storyline = ParseInfo::getStoryline($crawler);
            echo json_encode($storyline);
            die();
        case 'img':
            $img = ParseInfo::getImg($crawler);
            echo json_encode($img);
            die();
        case 'refreshAll':
            $refreshAll = ParseInfo::getAll($crawler);
            echo json_encode($refreshAll);
            die();

    }
//    if ($_POST['name'] == 'originTitle') {
//        $originTitle = ParseInfo::getOriginTitle($crawler);
//        echo json_encode($originTitle);
//        die();
//    }
//    if($_POST['name'] == 'rating'){
//        $rating = ParseInfo::getRating($crawler);
//        echo json_encode($rating);
//        die();
//    }
//    if($_POST['name'] == 'runtime'){
//        $runtime = ParseInfo::getRuntime($crawler);
//        echo json_encode($runtime);
//        die();
//    }
//    if($_POST['name'] == 'storyline'){
//        $storyline = ParseInfo::getStoryline($crawler);
//        echo json_encode($storyline);
//        die();
//    }
//    if($_POST['name'] == 'img'){
//        $img = ParseInfo::getImg($crawler);
//        echo json_encode($img);
//        die();
//    }
//    if($_POST['name'] == 'refreshAll'){
//        $img = ParseInfo::getAll($crawler);
//        echo json_encode($img);
//        die();
//    }
}