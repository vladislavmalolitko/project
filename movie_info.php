<?php

use Classes\Categories;
use Classes\FileCheck;
use Classes\Movies;
use Classes\ParseInfo;

require_once "config.php";

if (empty($_SESSION)) {
    header("Location: /register.php");
    die();
}

if (!empty($_GET['link'])) {
    $_SESSION['link'] = $_GET['link'];
}
if (isset($_POST['saveAddingMovie'])) {
    if (!empty($_FILES['file']['name'])) {
        $destinationDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . $_ENV['FILE_UPLOADS_DIR'];
        $checkType = FileCheck::checkType($_FILES['file']['type']);
        $checkError = FileCheck::checkError($_FILES['file']['error']);
        $checkSize = FileCheck::checkSize($_FILES['file']['size']);
        if (!file_exists($destinationDir)) {
            mkdir($_ENV['FILE_UPLOADS_DIR'], 0777, true);
        }
        if (!$checkType && !$checkError && !$checkSize) {
            $destinationName = $destinationDir . DIRECTORY_SEPARATOR . $_FILES['file']['name'];
            $destinationName = FileCheck::checkName($destinationName);
            if (move_uploaded_file($_FILES['file']['tmp_name'], $destinationName)) {
                $baseName = pathinfo($destinationName, PATHINFO_BASENAME);
                $destinationName = $_ENV['FILE_UPLOADS_DIR'] . DIRECTORY_SEPARATOR . $baseName;
                $_POST['img'] = $destinationName;
            }
        } else {
            if ($checkType) {
                $errors[] = $checkType;
            }
            if ($checkError) {
                $errors[] = $checkError;
            }
            if ($checkSize) {
                $errors[] = $checkSize;
            }
        }
    }
    $resultUpdates = Movies::updateMovie(
        $_POST['title'],
        $_POST['year'],
        $_POST['category'],
        $_SESSION['user_id'],
        $_SESSION['link'],
        $_POST['img'],
        $_POST['storyline'],
        $_POST['rating'],
        $_POST['originTitle'],
        $_POST['runtime']);
}

if (isset($_POST['saveMovie'])) {
    $resultUpdates = Movies::addMovie(
        $_POST['title'],
        $_SESSION['link'],
        $_POST['img'],
        $_SESSION['user_id'],
        $_POST['category'],
        $_POST['storyline'],
        $_POST['rating'],
        $_POST['originTitle'],
        $_POST['runtime'],
        $_POST['year']);
    $availabilityMovie = true;
}

$categories = Categories::getCategory();

$availabilityMovie = (Movies::getFullMovieInfo($_SESSION['user_id'], $_SESSION['link']));

if ($availabilityMovie) {
    $movieInfo = [
        'img' => $availabilityMovie['movie_img'],
        'title' => $availabilityMovie['movie_title'],
        'originTitle' => $availabilityMovie['movie_origin_title'],
        'year' => $availabilityMovie['movie_year'],
        'rating' => $availabilityMovie['movie_rating'],
        'runtime' => $availabilityMovie['movie_runtime'],
        'storyline' => $availabilityMovie['movie_storyline'],
        'category' => $availabilityMovie['movie_category_id']
    ];
} else {
    $crawler = ParseInfo::getCrawler($_SESSION['link']);
    if ($crawler) {
        $movieInfo = ParseInfo::getAll($crawler);
        if (isset($_POST['addMovie'])) {
            $statusAdding = Movies::addMovie(
                $movieInfo['title'],
                $_SESSION['link'],
                $movieInfo['img'],
                $_SESSION['user_id'],
                $movieInfo['category'],
                $movieInfo['storyline'],
                $movieInfo['rating'],
                $movieInfo['originTitle'],
                $movieInfo['runtime'],
                $movieInfo['year']);
        }
    }
}

require_once TEMPLATES_PATH . "info_page.php";
