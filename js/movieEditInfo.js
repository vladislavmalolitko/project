$(document).ready(function () {
    $('.refresh').click(function () {
        let name = $(this).attr('name')
        $.ajax({
            url: "refreshData.php",
            data: {"name": name, "link": $(this).val()},
            dataType: "json",
            type: "post",
            success: function (data) {
                if (name === 'storyline') {
                    $("textarea[name='" + name + "']").val(data)
                }
                if (name === 'img') {
                    $("img[id='" + name + "']").attr("src", data)
                    $("input[name='img']").val(data['img'])
                }
                if (name === 'category') {
                    $("select[name='category']").val(data)
                }
                if (name === 'refreshAll') {
                    $("input[name='title']").val(data['title'])
                    $("input[name='year']").val(data['year'])
                    $("input[name='img']").val(data['img'])
                    $("img[id='img']").attr("src", data['img'])
                    $("input[name='originTitle']").val(data['originTitle'])
                    $("input[name='runtime']").val(data['runtime'])
                    $("input[name='rating']").val(data['rating'])
                    $("textarea[name='storyline']").val(data['storyline'])
                    $("select[name='category']").val(data['category'])
                }
                $("input[name='" + name + "']").val(data)
            }
        });
    });
});

var image
$(document).ready(function () {
    $("#drop-area").on('dragenter', function (e) {
        e.preventDefault()
        $(this).css('background', '#BBD5B8')
    });

    $("#drop-area").on('dragover', function (e) {
        e.preventDefault()
    });

    $("#drop-area").on('drop', function (e) {
        $(this).css('background', '#D8F9D3');
        e.preventDefault()
        image = e.originalEvent.dataTransfer.files
        document.getElementById("drop-area-text").innerHTML = image[0].name
    });
    $("#drop-area").click(function (e) {
        checkCountImg(e, image)
    })
});

function checkCountImg(e, image) {
    if (image[1]) {
        alert('More files')
    } else {
        if (checkImage(image[0])) {
            uploadImage(e, image[0])
        }
    }
}

function checkImage(image) {
    let types = ['image/jpeg', 'image/png']
    if ((image.size < 1) && (image.size > 2097152)) {
        alert('File is large')
        return false
    }
    if (!types.includes(image.type)) {
        alert('File should be image')
        return false
    }
    return true
}

function uploadImage(e, file) {
    e.preventDefault()
    var formImage = new FormData();
    formImage.append('file', image[0]);
    console.log(formImage)
    $.ajax({
        url: "movie_info.php",
        type: "POST",
        data: formImage,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            // $('#formAction').submit();
        }
    })
}