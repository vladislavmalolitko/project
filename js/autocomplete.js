$(function () {
    $('#search_field').on('input', function () {
        $.ajax({
            url: "autocomplete_ajax.php",
            data: {"value": $("#search_field").val()},
            dataType: "json",
            type: "post",
            success: function (data) {
                console.log(data)
                $('#search_field').autocomplete({
                    source: data
                })
            }
        })
    })
})

$(document).ready(function () {

    var features = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace('search_query'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 20,
        remote: {
            url: 'searchIMDB_ajax.php?keyword=%QUERY%',
            wildcard: '%QUERY%'
        }
    });
    features.initialize();


    $('.typeaheadDiv #searchInIMDB').typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        },
        {
            name: 'features',
            displayKey: 'title',
            limit: 10,
            source: features.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'unable to find any Movies that match the current query',
                    '</div>'
                ].join('\n'),
                header: '<h3 class="league-name">Features</h3>',
                suggestion: Handlebars.compile('<div class="divContainer container"><div class="divItem row' +
                    ' justify-content-md-center mx-auto"><span' +
                    ' class="col-md-5"><img src="{{img}}" class="small-thumb"></span> <span' +
                    ' class="col-md-7"><a class="nav-link"' +
                    ' href="movie_info.php?link={{href}}">{{title}}</a></span></div></div>')
            }
        }
    );
})


function closeAlert() {
    $('.alert').hide();
}

setTimeout(closeAlert, 3000);


