<?php

require_once "config.php";

use Classes\Categories;
use Classes\HomePage;
use Classes\Movies;

if (empty($_SESSION)) {
    header("Location: /register.php");
    die();
}

if (isset($_POST['logout'])) {
    $_SESSION = [];
    header("Location: /register.php");
    die();
}

if (isset($_POST['view'])) {
    header('Location: /movie_info.php?link=' . $_POST['view']);
    die();
}
if (isset($_POST['delete'])) {
    Movies::deleteMovie($_POST['delete']);
}
$userId = $_SESSION['user_id'];

if (isset($_POST['searchInDb'])) {
    $movieName = $_POST['searchInDb'];
    $movies = Movies::getSearchMovie($userId, $movieName);
    header('Location: /movie_info.php?link=' . $movies['movie_href']);
    die();
}
if (isset($_POST['query'])) {
    echo HomePage::autoload($_POST['query']);
}


$categories = Categories::getCategory();
if (Movies::checkMovie($userId) != 0) {
    if (empty($_GET['link']) && empty($_SESSION['categoryId'])) {
        $_SESSION['categoryId'] = 'all';
    } else {
        $_SESSION['categoryId'] = (empty($_GET['link'])) ? $_SESSION['categoryId'] : $_GET['link'];
    }
    $countPages = HomePage::getCountPages($userId, $_SESSION['categoryId']);
    if (!empty($countPages)) {
        //PAGINATION
        if (!isset($_POST['next']) && !isset($_POST['prev'])) {
            if (empty($_POST['page'])) {
                $_SESSION['page'] = 1;
            } else {
                $_SESSION['page'] = $_POST['page'];
            }
        }

        if (isset($_POST['next'])) {
            ((int)$_SESSION['page'] == $countPages) ?: $_SESSION['page']++;
        }
        if (isset($_POST['prev'])) {
            if ($_SESSION['page'] > 1) {
                $_SESSION['page'] -= 1;
            } else {
                $_SESSION['page'] = 1;
            }
        }
        //END PAGINATION
        $status = $_SESSION['page'];
        $movies = HomePage::getMovie($userId, $_SESSION['categoryId'], $_SESSION['page']);
    }

}
require_once TEMPLATES_PATH . "home_page.php";
