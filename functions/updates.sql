CREATE TABLE `users` (
                         `user_id` int(20) NOT NULL AUTO_INCREMENT,
                         `user_name` varchar(255) NOT NULL,
                         `user_email` varchar(255) NOT NULL,
                         `user_password` varchar(255) NOT NULL,
                         `user_watching_time` float(25,2) NOT NULL,
                         `user_code_restore` int(6) DEFAULT NULL,
                         PRIMARY KEY (`user_id`),
                         UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

CREATE TABLE `movies` (
                          `movie_id` int(20) NOT NULL AUTO_INCREMENT,
                          `movie_title` varchar(255) NOT NULL,
                          `movie_href` varchar(255) NOT NULL,
                          `movie_img` varchar(255) NOT NULL,
                          `movie_category_id` int(20) NOT NULL,
                          `movie_user_id` int(20) NOT NULL,
                          `movie_storyline` varchar(10000) NOT NULL,
                          `movie_rating` float(2,1) NOT NULL,
                          `movie_origin_title` varchar(255) NOT NULL,
                          `movie_runtime` varchar ( 20 ) NOT NULL,
                          `movie_year` varchar(20) NOT NULL,
                          PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

CREATE TABLE `categories` (
                              `category_id` int(20) NOT NULL AUTO_INCREMENT,
                              `category_name` varchar(255) NOT NULL,
                              PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
