<?php

require_once "config.php";

use Classes\Users;
use Classes\Verification;

$error = [];
if (isset($_POST['restore']) && ($_POST['restore'] == 'restorePassword')) {
    if (empty($_POST['email'])) {
        $error['email'] = "Field should be filled";
    } else {
        if ($rezError = Verification::checkEmail($_POST['email'], false)) {
            $error['email'] = $rezError;
        }
        if (!Verification::checkingUsedEmail($_POST['email'])) {
            $error['email'] = "No user found with this email";
        }
    }

    if (empty($error)) {
        $_SESSION['email'] = $_POST['email'];
        $messageCode = '';
        for ($i = 0; $i < 6; $i++) {
            $messageCode .= rand(0, 9);
        }
        var_dump($messageCode);
        try {
            mail($_POST['email'], 'Code restore', (int)$messageCode);
            $addCodeRestore = Users::createCodeRestore($_POST['email'], $messageCode);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}

if (isset($_POST['restore']) && ($_POST['restore'] == 'checkCode')) {
    if (empty($_POST['code'])) {
        $error['code'] = "Field should be filled";
    } else {
        if (!(Verification::checkCode($_SESSION['email'], $_POST['code']))) {
            $error['code'] = "Code didn't match";
        }
    }
}

if (isset($_POST['restore']) && ($_POST['restore'] == 'changePassword')) {
    if (empty($_POST['password'])) {
        $error['password'] = "Field should be filled";
    } else {
        if ($rezError = (Verification::checkPassword($_POST['password']))) {
            $error['password'] = $rezError;
        }
        if ($_POST['password'] != $_POST['confirm_password']) {
            $error['confirm_password'] = "Password don't match";
        }
    }
    if (empty($_POST['confirm_password'])) {
        $error['confirm_password'] = "Field should be filled";
    }
    if (empty($error)) {
        $userUpdate = Verification::updatePassword($_SESSION['email'], $_POST['password']);

        $userId = Users::getUserIdByEmail($_SESSION['email']);
        $_SESSION['user_id'] = (int)$userId;
        header("Location: /index.php");
        die();
    }
}
require_once TEMPLATES_PATH . "restore_form.php";
